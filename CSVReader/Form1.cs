using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// This is the code for your desktop app.
// Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.

namespace CSVReader
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // Click on the link below to continue learning how to build a desktop app using WinForms!
            System.Diagnostics.Process.Start("http://aka.ms/dotnet-get-started-desktop");

        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Thanks!");
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btn1_Click(object sender, EventArgs e)
        {
           
            string fileChoice = ofd1.FileName;

            var lines = File.ReadAllLines(fileChoice);
            if (File.Exists(fileChoice)) {
                foreach (var line in lines)
                {
                    lineProcess(line, 1);
                    lineProcess(line, 2);
                    lineProcess(line, 3);
                }
            }
        }
        private void lineProcess(string singleLine,int phoneCount) {
            var col = singleLine.Split(',');
            textBox1.Text = col[0];
            textBox2.Text = col[1];
            textBox3.Text = col[2];
            textBox4.Text = col[3];


        }

        private void button1_Click_1(object sender, EventArgs e)
        {

            Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ofd1.InitialDirectory = @"D:\";
            ofd1.ShowDialog();
            textBox5.Text = ofd1.FileName.ToString();

        }


    }
}
